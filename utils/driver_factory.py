from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager


class DriverFactory:

    @staticmethod
    def get_driver(browser):
        if browser == "chrome":
            options = webdriver.ChromeOptions()
            options.add_argument("start-maximized")
            return webdriver.Chrome(ChromeDriverManager().install(), options=options)
        elif browser == "firefox":
            options = webdriver.FirefoxOptions()
            options.add_argument("start-maximized")
            return webdriver.Firefox(executable_path=GeckoDriverManager().install(), options=options)
        elif browser == "ie":
            desired_cap = {
                'os_version': '10',
                'resolution': '1920x1080',
                'browser': 'IE',
                'browser_version': '11.0',
                'os': 'Windows',
                'name': 'Sign Document',
                'build': 'Main Build',
                'browserstack.console': "errors",
                'browserstack.networkLogs': 'true',
            }
            return webdriver.Remote(
                command_executor='https://adam_QkLqzm:tqpGNk7HoWwC5LCu82ex@hub-cloud.browserstack.com/wd/hub',
                desired_capabilities=desired_cap)
        raise Exception("Invalid browser")
