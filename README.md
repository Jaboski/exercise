# Exercise

1. Screenshot of an element works locally but method used for that action does not work in browserstack testing. I handled that by screenshoting a full page during browserstack testing. If I had more time I would have cut element out of the photo using Image class/module.

2. Browserstack testing:
In test class, use 'browserstack_setup' parameter for fixture and test_sign_document test.

3. For linux testing:
It could be done with linux server and selenium grid. 
- start the hub on windows using selenium-server-standalone
- start the node on linux server with host details for hub (same like we have for grid localhost:4444) 
- in tests, for webdriver object provide RemoteWebDriver (http://localhost:4444/wd/hub) with capabilities.
