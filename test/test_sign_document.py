import pytest

from base_test import BaseTest
from page_object_pattern.sign_document import DocumentPage


@pytest.mark.usefixtures("setup")
class TestSignDocument(BaseTest):

    def test_sign_document(self, setup):
        sign_document = DocumentPage(self.driver)
        sign_document.navigate_to_main_page()
        sign_document.set_document_name('Adam')
        sign_document.go_next_page()
        sign_document.screenshot_confirmation()
        sign_document.sign_the_document()
        sign_document.confirm_signed_document()
