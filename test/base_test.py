import pytest

from utils.driver_factory import DriverFactory


class BaseTest:

    @pytest.fixture()
    def setup(self):
        self.driver = DriverFactory.get_driver("chrome")
        self.driver.implicitly_wait(10)
        yield
        self.driver.quit()

    @pytest.fixture()
    def browserstack_setup(self):
        self.driver = DriverFactory.get_driver("ie")
        yield
        self.driver.quit()
