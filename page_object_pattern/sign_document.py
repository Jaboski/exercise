import time

from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class DocumentPage:

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10, 0.5)
        self.url = "https://staging.scrive.com/t/9221714692410699950/7348c782641060a9"
        self.document_name = 'name'
        self.next_btn = "//*[contains(text(),'Next')]"
        self.sign_btn = ".sign-button"
        self.confirmation_modal = "div.sign.above-overlay"
        self.signed_text = ".s-header-doc-signed span"
        self.document_signed = "//*[contains(text(),'Document signed!')]"

    def navigate_to_main_page(self):
        self.driver.get(self.url)
        self.wait.until(expected_conditions.presence_of_element_located((By.ID, self.document_name)))

    def set_document_name(self, given_name):
        self.driver.find_element_by_id(self.document_name).send_keys(given_name)

    def go_next_page(self):
        btn = self.driver.find_element_by_xpath(self.next_btn)
        self.wait.until(expected_conditions.element_to_be_clickable((By.XPATH, self.next_btn)))
        btn.click()

    def screenshot_confirmation(self):
        self.wait.until(expected_conditions.presence_of_element_located((By.CSS_SELECTOR, self.sign_btn)))
        time.sleep(1)
        try:
            modal = self.driver.find_element_by_css_selector(self.confirmation_modal)
            modal.screenshot("./image.png")
        except WebDriverException:
            print('WebDriverException occurred, taking full page screenshot..')
            self.driver.get_screenshot_as_file('./image.png')

    def sign_the_document(self):
        btn = self.driver.find_element_by_css_selector(self.sign_btn)
        self.wait.until(expected_conditions.element_to_be_clickable((By.CSS_SELECTOR, self.sign_btn)))
        btn.click()

    def confirm_signed_document(self):
        self.wait.until(expected_conditions.presence_of_element_located((By.CSS_SELECTOR, self.signed_text)))
        signed_text = self.driver.find_element_by_css_selector(self.signed_text).text
        assert signed_text == 'Document signed!'
